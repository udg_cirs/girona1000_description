# Changelog

## [24.1.0] - 30-01-2024

* Updated for release 24.1.0

## [20.10.0] - 26-10-2020

* First release
